let is_empty = function
    | [] -> true
    | _ -> false

let rec is_in_list l x = match l with
    | [] -> false
    | head::tail -> (head = x) || (is_in_list tail x)

let rec iter_liste l le n = match l with
    | head::tail when ((is_in_list le head ) = true)
        && ((is_in_list n head) = false) -> (iter_liste tail le (n @ [head]))
    | head::tail when (((is_in_list le head ) = false)
        || ((is_in_list le head ) = true) && ((is_in_list n head) = true))-> (iter_liste tail le n)
    | _ -> n

let crossover l le =
    if (is_empty l) = true || (is_empty le) = true then []
    else (iter_liste l le [])

(******************************************************************************)

let rec print_debug l = match l with
    | [] -> ()
    | h::t -> (Printf.printf "%c " h ; print_debug t)

let () =
    print_endline "Test with ['a', 'b', 'c'] ['a', 'c']";
    print_debug (crossover ['a'; 'b'; 'c'] ['a'; 'c']);
    print_char '\n';
    print_endline "Test with ['a', 'b', 'c', 'a'] ['a', 'c']";
    print_debug (crossover ['a'; 'b'; 'c'; 'a'] ['a'; 'c']);
    print_char '\n';
    print_endline "Test with ['a', 'b', 'c', 'a', 'b', 'k'] ['a', 'c', 'a', 'k']";
    print_debug (crossover ['a'; 'b'; 'c'; 'c'; 'a'; 'b'; 'k'] ['a'; 'c'; 'a'; 'k']);
    print_char '\n';
    print_endline "Test with ['a', 'b'i, 'c', 'a', 'b', 'k'] []";
    print_debug (crossover ['a'; 'b'; 'c'; 'a'; 'b'; 'k'] []);
    print_char '\n’;
