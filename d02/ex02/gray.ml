let push_front c s = String.make 1 c ^ s

let rec ( ++ ) l1 l2 = match l1 with
    | [] -> l2
    | h::t -> h::(t ++ l2);;

let rec rev l acc = match l with
    | [] -> acc
    | h :: t -> rev t (h :: acc)

let rec map f l = match l with
    | [] -> []
    | h :: t -> f h :: map f t

let rec ft_rec n = match n with
    | 0 -> []
    | 1 -> ["0";"1"]
    | _ -> let g = ft_rec (n - 1) in
            (map (push_front '0') g) ++ (rev (map (push_front '1') g) [])

let gray n =
    print_endline ( String.concat " " (ft_rec n) )

(******************************************************************************)

let () =
    for i=0 to 5 do
        gray i
    done
