type phosphate = string

type deoxyribose = string

type nucleobase = A | T | C | G | None

type nucleotide = {
    p : phosphate ;
    d : deoxyribose ;
    n : nucleobase
}

let rec get_nucleobase n = match n with
    | 'A' -> A
    | 'T' -> T
    | 'C' -> C
    | 'G' -> G
    | _ -> None

let generate_nucleotide nucleobase =
    let nucleo = {
        p = "phosphate" ;
        d = "deoxyribose" ;
        n = (get_nucleobase nucleobase)
    }
    in nucleo


(* * *)

let rec get_char n = match n with
    | A -> "A"
    | T -> "T"
    | C -> "C"
    | G -> "G"
    | None -> "None"

let print_nucleo n =
    Printf.printf "   phosphate = %s\n" n.p;
    Printf.printf "   desoxyribose = %s\n" n.d;
    Printf.printf "   nucleobase = %s\n" (get_char n.n)

let () =
    print_endline "Test with A" ;
    print_nucleo (generate_nucleotide 'A');
    print_endline "Test with T" ;
    print_nucleo (generate_nucleotide 'T');
    print_endline "Test with C" ;
    print_nucleo (generate_nucleotide 'C');
    print_endline "Test with G" ;
    print_nucleo (generate_nucleotide 'G');
    print_endline "Test with E" ;
    print_nucleo (generate_nucleotide 'E')
