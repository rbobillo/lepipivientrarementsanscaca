let is_empty = function
    | [] -> true
    | _ -> false

let rec iter liste c x = match liste with
    | h::t when h = x -> (iter t (c + 1) x)
    | _ -> (c, x)

let rec compress l x tup = match l with
    | h::t when h = x -> (compress t h tup)
    | h::t when h <> x -> (compress t h (tup @ [(iter t 1 h)]))
    | _ -> tup

let encode l =
    if (is_empty l) = true
    then ([])
    else
    (compress l "\n" [])

let ft_is_one = function
    | 1 -> true
    | _ -> false

let ft_convert t = match t with
    | (x, y) -> [(string_of_int x) ; y]

let rec convert_to_list l r = match l with
    | [] -> r
    | h::t -> convert_to_list t (r @ (ft_convert h))

let rec convert_to_string s r = match s with
    | [] -> r
    | h::t -> convert_to_string t (r^h)

let rec generate n s =
    if (ft_is_one n)
    then (convert_to_string s "")
    else
    (generate (n-1) (convert_to_list (encode s) []))

let sequence n = match n with
    | y when y > 0 -> generate n ["1"]
    | _ -> ""

(* **** *)

let () =
	for i=(-1) to 10 do
		Printf.printf "Test with %d: %s\n" i (sequence (i));
	done
