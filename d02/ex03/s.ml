let rec rev l acc = match l with
    | [] -> acc
    | h :: t -> rev t (h :: acc)

let encode lst =
    let rec aux count acc = function
        | [] -> []
        | [x] -> (count+1, x) :: acc
        | a :: (b :: _ as t) ->
                if a = b then (aux (count + 1) acc t)
                else (aux 0 ((count+1,a) :: acc) t)
    in
    rev (aux 0 [] lst) []

(******************************************************************************)

let print_tuple_char c = match c with
| (x, y) -> (print_int x ; print_char y)

let rec print_lst_char lst = match lst with
    | h::t -> (print_tuple_char h ; print_lst_char t)
    | [] -> print_char '\n'

let print_tuple_int c = match c with
| (x, y) -> (print_int x ; print_int y)

let rec print_lst_int lst = match lst with
    | h::t -> (print_tuple_int h ; print_lst_int t)
    | [] -> print_char '\n'

open Printf
let () =
    printf "Testing [caaaaaabb]: " ;
    print_lst_char (encode ['c';'a';'a';'a';'a';'a';'a';'b';'b']) ;
    printf "Testing [112222213]: " ;
    print_lst_int (encode [1;1;2;2;2;2;2;1;3]);
