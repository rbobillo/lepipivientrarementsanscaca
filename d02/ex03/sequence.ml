let string_of_char c =
    (string_of_int ((int_of_char c) - 48))

let append i c s =
     s ^ ((string_of_int i) ^ (string_of_char c))

let rec iter str i x m s = match str.[i] with
    | c when i = ((String.length ) str - 1) -> append (x + 1) m s
    | c when c = m -> (iter str (i + 1) (x + 1) c s)
    | c when c <> m -> (iter str (i + 1) 1 c (append x m s))
    | _ -> s

let sequence n =
    if n = 0 then ""
    else
        let rec loop n str =
            if n = 1 then str
            else if str = "21" then loop (n - 1) "1211"
            else loop (n - 1) (iter str 0 0 str.[0] "")
        in
        loop n "1"

let () =
    for i=0 to 15 do
        print_endline (sequence i)
    done
