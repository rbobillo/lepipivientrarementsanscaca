type phosphate = string

type deoxyribose = string

type nucleobase = A | T | C | G | None

type nucleotide = {
    p : phosphate ;
    d : deoxyribose ;
    n : nucleobase
}

let rec get_nucleobase n = match n with
    | 'A' -> A
    | 'T' -> T
    | 'C' -> C
    | 'G' -> G
    | _ -> None

let generate_nucleotide nucleobase =
    let nucleo = {
        p = "phosphate" ;
        d = "deoxyribose" ;
        n = (get_nucleobase nucleobase)
    }
    in nucleo

(* * *)

type helix = nucleotide list

let random n = match n with
    | 0 -> A
    | 1 -> T
    | 2 -> C
    | 3 -> G
    | _ -> None

let generate_random_nucleotide =
    Random.self_init ();
    let nucleo = {
        p = "phosphate" ;
        d = "deoxyribose" ;
        n = (random (Random.int 1000 mod 4))
    } in nucleo

let rec get_char n = match n with
    | A -> "A"
    | T -> "T"
    | C -> "C"
    | G -> "G"
    | None -> "None"

let generate_helix n =
    let rec gen n liste =
        if n = 0 then liste
        else gen (n - 1) (generate_random_nucleotide::liste)
    in gen n []

let helix_to_string liste =
    let rec to_string liste str = match liste with
        | head::tail -> to_string tail ((get_char head.n) ^ str)
        | _ -> str
    in to_string liste ""

let convert_nuc nuc = match nuc with
    | A -> T
    | T -> A
    | C -> G
    | G -> C
    | _ -> None

let get_nucleotide n =
    let nucleo = {
        p = "phosphate" ;
        d = "deoxyribose" ;
        n = n
    } in nucleo

let complementary_helix liste =
    let rec new_liste liste l = match liste with
        | head::tail -> new_liste tail ((get_nucleotide (convert_nuc head.n))::l)
        | _ -> l
    in new_liste liste []

(* * *)


let print_nucleo n =
    Printf.printf "   phosphate = %s\n" n.p;
    Printf.printf "   desoxyribose = %s\n" n.d;
    Printf.printf "   nucleobase = %s\n" (get_char n.n)

let () =
    print_endline "Test with 5" ;
    let l = generate_helix 5 in
    print_endline (helix_to_string l);
    print_endline (helix_to_string (complementary_helix l));
    print_endline "Test with 10" ;
    let le = generate_helix 10 in
    print_endline (helix_to_string le);
    print_endline (helix_to_string (complementary_helix le))
