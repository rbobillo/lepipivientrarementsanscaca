let rec ft_power x n =
    if n = 0 then 1
    else x * (ft_power x (n - 1) )

(******************************************************************************)

let ft_ppow x n =
    Printf.printf "%d^%d: " x n ; print_int (ft_power x n) ; print_char '\n'

let () =
    ft_ppow 6 0 ;
    ft_ppow 6 1 ;
    ft_ppow (-6) 2 ;
    ft_ppow 6 2 ;
    ft_ppow 5 5 ;
    ft_ppow 0 0 ;
    ft_ppow 6 3
