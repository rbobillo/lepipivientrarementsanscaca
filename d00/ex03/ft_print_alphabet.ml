let ft_print_alphabet () =
    let rec ft_rec x =
        if x <= ( int_of_char 'z') then
            begin print_char (char_of_int x) ; ft_rec (x + 1) end
        else
            print_char '\n'
    in
    ft_rec ( int_of_char 'a' )

(******************************************************************************)

let () = ft_print_alphabet ()
