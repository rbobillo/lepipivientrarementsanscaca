let ft_print_comb2 () =
    let rec ft_print i j k l =
        if i = k && j = l then
            ft_print i j (k+1) 0
        else (
            print_int i ;
            print_int j ;
            print_char ' ' ;
            print_int k ;
            print_int l ;
            if i = 9 && j = 8 && k = 9 && l = 9 then
                print_char '\n'
            else (
                print_char ',' ;
                print_char ' ' ;
                if l <> 9
                then
                    ( ft_print i j k (l+1) ) ;
                if k <> 9 && l = 9
                then
                    ( ft_print i j (k+1) 0 ) ;
                if j <> 9 && k = 9 && l = 9
                then
                begin
                    if (j+2) > 9 then
                        ft_print i (j+1) i (j+1)
                    else
                        ft_print i (j+1) i (j+2)
                end ;
                if i <> 9 && j = 9 && k = 9 && l = 9 then
                    ft_print (i+1) 0 (i+1) 1
            )
        )
    in
        ft_print 0 0 0 1

(******************************************************************************)

let () = ft_print_comb2 ()
