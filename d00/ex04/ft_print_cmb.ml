let ft_print_comb () =
    let rec ft_rec x y z =
        print_int x ;
        print_int y ;
        print_int z ;
        if x = 7 && y = 8 && z = 9 then
            print_string "\n"
        else (
            print_string ", " ;
            if z <> 9 then
                ( ft_rec x y (z+1) ) ;
            if z = 9 && y <> 8 then
                ( ft_rec x (y+1) (y+2) ) ;
            if z = 9 && y = 8 then
                ( ft_rec (x+1) (x+2) (x+3) )
        )
    in
        ft_rec 0 1 2

(******************************************************************************)

let () =
    ft_print_comb ()
