let rec ft_rot_n n str =
    let rot c =
        if c = 'z' then
            'a'
        else if c = 'Z' then
            'A'
        else if (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') then
            ( char_of_int (int_of_char c + 1) )
        else
            c
    in
        if n > 0 then
            ( ft_rot_n (n - 1) (String.map rot str) )
        else
            str

(******************************************************************************)

let () =
    print_endline (ft_rot_n 1 "abcdefghijklmnopqrstuvwxyz") ;
    print_endline (ft_rot_n 13 "abcdefghijklmnopqrstuvwxyz") ;
    print_endline (ft_rot_n 42 "0123456789") ;
    print_endline (ft_rot_n 2 "OI2EAS67B9") ;
    print_endline (ft_rot_n 0 "Damned !") ;
    print_endline (ft_rot_n 42 " " );
    print_endline (ft_rot_n 1 "NBzlk qnbjr !" )
