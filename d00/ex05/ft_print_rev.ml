let ft_print_rev s =
    let rec ft_rec s n =
        if n >= 0 then
            ( print_char (String.get s n) ; ft_rec s (n - 1) )
        else
            print_char '\n'
    in
        ft_rec s ((String.length s) - 1)

(******************************************************************************)

let () =
    print_string "Hello world !: " ; ft_print_rev "Hello world !" ;
    print_string "(null): " ; ft_print_rev ""
