let ft_string_all check s =
    let rec check_at_index check s n =
        if n > 0 && check (String.get s n) then
            (check_at_index check s (n - 1))
        else if n == 0 then
            (check (String.get s n))
        else
            false
    in
        check_at_index check s ((String.length s) - 1)

let is_digit c = c >= '0' && c <= '9'

(******************************************************************************)

let check s = match ft_string_all is_digit s with
    true -> print_endline "true"
    | _ -> print_endline "false"

let () =
    print_string "1234:        " ; check "1234" ;
    print_string "12A4:        " ; check "12A4" ;
    print_string "0123456789:  " ; check "0123456789" ;
    print_string "O12EAS67B9:  " ; check "O12EAS67B9"
