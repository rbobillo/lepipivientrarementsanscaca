let ft_test_sign x =
    if x >= 0 then
        print_endline "positive"
    else
        print_endline "negative"

(******************************************************************************)

let () =
    print_string "ft_test_sign [42]   -> " ; ft_test_sign 42 ;
    print_string "ft_test_sign [0]    -> " ; ft_test_sign 0 ;
    print_string "ft_test_sign [-42]  -> " ; ft_test_sign (-42) ;
    print_string "ft_test_sign [-0]   -> " ; ft_test_sign (-0)
