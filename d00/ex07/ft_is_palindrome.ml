let ft_is_palindrome s =
    let rec ft_rec s len i =
        if i < ((len / 2) + (len mod 2)) then
            if (String.get s i) == (String.get s (len - i)) then
                ( ft_rec s len (i + 1) )
            else
                false
        else
            if (String.get s i) == (String.get s (len - i)) then
                true
            else
                false
    in
        ft_rec s ((String.length s) - 1) 0

(******************************************************************************)

let check s = match ft_is_palindrome s with
    true -> print_endline "true"
    | _ -> print_endline "false"

let () =
    print_string "radar:  " ; check "radar" ;
    print_string "kayak:  " ; check "kayak" ;
    print_string "yolo:   " ; check "yolo" ;
    print_string "fdp:    " ; check "fdp"
