let abs x = if x < 0. then x *. (-1.) else x

let leibniz_pi delta =
    let rec ft_sum f min res precision accu =
        if abs (res -. (4. *. atan 1.)) <= precision then
            accu
        else
            ft_sum f (min +. 1.) (res +. (f min)) precision (accu + 1)
    in
    if delta < 0. then
        -1
    else
        ft_sum (fun i -> (4. *. ((((-1.) ** i ) /. ((2. *. i) +. 1.))))) 0. 0. delta 0

(******************************************************************************)

let () =
    Printf.printf "leibniz_pi (-1.)       = %d\n" ( leibniz_pi (-1.) ) ;
    Printf.printf "leibniz_pi 0.5         = %d\n" ( leibniz_pi 0.5 ) ;
    Printf.printf "leibniz_pi 0.1         = %d\n" ( leibniz_pi 0.1 ) ;
    Printf.printf "leibniz_pi 0.01        = %d\n" ( leibniz_pi 0.01 ) ;
    Printf.printf "leibniz_pi 0.001       = %d\n" ( leibniz_pi 0.001 ) ;
    Printf.printf "leibniz_pi 0.0001      = %d\n" ( leibniz_pi 0.0001 ) ;
    Printf.printf "leibniz_pi 0.00001     = %d\n" ( leibniz_pi 0.00001 ) ;
