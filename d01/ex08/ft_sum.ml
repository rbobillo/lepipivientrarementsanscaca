let ft_sum f min max =
    let rec ft_loop f min max res =
        if max < min then
            nan
        else (
            if max = min then
                (f max) +. res
            else
                ft_loop f min (max - 1) ((f max) +. res)
        )
    in
    ft_loop f min max 0.

(******************************************************************************)

let () =
    print_float (ft_sum (fun i -> float_of_int (i * i)) 10 1) ; print_char '\n' ;
    print_float (ft_sum (fun i -> float_of_int (i * i)) 1 10) ; print_char '\n' ;
    print_float (ft_sum (fun i -> float_of_int (i * i)) 2 13) ; print_char '\n' ;
    print_float (ft_sum (fun i -> float_of_int (i * i)) 3 10) ; print_char '\n' ;
    print_float (ft_sum (fun i -> float_of_int (i * i)) 12 25) ; print_char '\n'
