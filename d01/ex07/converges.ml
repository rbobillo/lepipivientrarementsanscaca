let rec converges f x n =
    if n < 0 then
        false
    else if x = (f x) then
        true
    else
        converges f (f x) (n - 1)

(******************************************************************************)

let check b = match b with
    true -> "true"
    | _ -> "false"

open Printf
let () =
   printf "%s\n" (check ( converges (( * ) 2) 2 5 )) ;
   printf "%s\n" (check ( converges (fun x -> x / 2) 2 3 )) ;
   printf "%s\n" (check ( converges (fun x -> x / 2) 2 2 )) ;
