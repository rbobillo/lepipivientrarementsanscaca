let repeat_string ?str:(s="x") y =
    let rec ft_rec str x y =
        if y = 0 then
            str
        else
            ft_rec (str ^ x) x (y - 1)
    in
    if y < 0 then
        "Error"
    else
       ft_rec "" s y

(******************************************************************************)

open Printf
let () =
    printf "%s\n" (repeat_string (-1)) ;
    printf "%s\n" (repeat_string 0) ;
    printf "%s\n" (repeat_string ~str:"Toto" 1) ;
    printf "%s\n" (repeat_string 2) ;
    printf "%s\n" (repeat_string ~str:"a" (5)) ;
    printf "%s\n" (repeat_string ~str:"what" 3)
