let rec iter f x n =
    if n < 0 then
        -1
    else if n = 0 then
        x
    else
        iter f (f x) (n - 1)

(******************************************************************************)

open Printf
let () =
    printf "%d\n" ( iter (fun x -> x * x) 2 4 ) ;
    printf "%d\n" ( iter (fun x -> x * 2) 2 4 )
