let repeat_x n =
    let rec ft_rec s n =
        if n < 0 then
            "Error"
        else if n = 0 then
            s
        else
            ft_rec (s ^ "x") (n - 1)
    in
    ft_rec "" n

(******************************************************************************)

open Printf
let () =
    printf "repeat_x (-1): %s\n" ( repeat_x (-1) ) ;
    printf "repeat_x 0:    %s\n" ( repeat_x 0 ) ;
    printf "repeat_x 1:    %s\n" ( repeat_x 1 ) ;
    printf "repeat_x 5:    %s\n" ( repeat_x 5 ) ;
    printf "repeat_x 42:   %s\n" ( repeat_x 42 )
