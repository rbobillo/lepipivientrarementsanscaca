let rec ackermann x y =
    if x < 0 || x < 0 then
        -1
    else
        if x=0 then
            (y+1)
        else
            if y=0 then
                (ackermann (x-1) 1)
            else
                (ackermann (x-1) (ackermann x (y-1)))

(******************************************************************************)

open Printf
let () =
    printf "%d\n" (ackermann (-1) 0) ;
    printf "%d\n" (ackermann 0 0) ;
    printf "%d\n" (ackermann 2 3) ;
    printf "%d\n" (ackermann 4 1)
