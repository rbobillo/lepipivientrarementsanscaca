let fibonacci n =
    let rec tail n x y =
        if n = 0 then x else tail (n - 1) y (x + y)
    in
    if n < 0 then -1 else tail n 0 1

(******************************************************************************)

open Printf
let () =
    for i=0 to 15 do
        printf "fibonacci(%d) = %d\n" i (fibonacci (i))
    done
