class virtual molecule (name) (atoms: Atom.atom list) =
    object (self)
        val _name : string = name
        val _formula : string =
            let rec rle curr = function
                | x :: y :: ys when x#equals y -> rle (curr + 1) (y::ys)
                | x :: y :: ys -> (x, curr + 1) :: (rle 0 (y::ys))
                | x :: [] -> (x, curr + 1) :: []
                | [] -> []
            in let hillNotationOrder l r =
                if l#atomic_number = 6 || l#atomic_number = 1
                then if r#atomic_number = 6 then 1 else -1
                else if r#atomic_number = 6 || r#atomic_number = 1 then 1
                else compare l#symbol r#symbol
            in let sup acc (a, n) = acc ^ a#symbol ^ (string_of_int n)
            in List.fold_left sup "" (rle 0 (List.sort hillNotationOrder atoms))

        method name = _name
        method formula = _formula
        method to_string = _name ^ "(" ^ _formula ^ ")"
        method equals (other : molecule) = self#name = other#name && self#formula = other#formula
    end

class trinitrotoluene =
    object (self)
        inherit molecule "Trinitrotoluene" [ new Atom.nitrogen ; new Atom.nitrogen
                                           ; new Atom.nitrogen
                                           ; new Atom.oxygen ; new Atom.oxygen
                                           ; new Atom.carbon ; new Atom.carbon
                                           ; new Atom.hydrogen
                                           ; new Atom.oxygen ; new Atom.oxygen
                                           ; new Atom.oxygen ; new Atom.oxygen
                                           ; new Atom.hydrogen ; new Atom.hydrogen
                                           ; new Atom.hydrogen ; new Atom.hydrogen
                                           ; new Atom.carbon ; new Atom.carbon
                                           ; new Atom.carbon ; new Atom.carbon
                                           ; new Atom.carbon ]
    end


class water =
    object
        inherit molecule "water" [ new Atom.hydrogen
                                 ; new Atom.hydrogen
                                 ; new Atom.oxygen ]
    end

class carbon_dioxyde =
    object
        inherit molecule "carbon dioxyde" [ new Atom.carbon
                                          ; new Atom.oxygen
                                          ; new Atom.oxygen ]
    end

class methane =
    object
        inherit molecule "methane" [ new Atom.carbon
                                   ; new Atom.hydrogen
                                   ; new Atom.hydrogen
                                   ; new Atom.hydrogen
                                   ; new Atom.hydrogen ]
    end

class oxygen =
    object
        inherit molecule "oxygen" [ new Atom.oxygen
                                  ; new Atom.oxygen ]
    end

class ozone =
    object inherit molecule "ozone" [ new Atom.oxygen
                                    ; new Atom.oxygen ]
    end
