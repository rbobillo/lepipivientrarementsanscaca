let () =
    let t = new Molecule.trinitrotoluene in
    let w = new Molecule.water in
    let c = new Molecule.carbon_dioxyde in
    let m = new Molecule.methane in
    let ox = new Molecule.oxygen in
    let oz = new Molecule.ozone in
        print_endline (t # to_string) ;
        print_endline (w # to_string) ;
        print_endline (c # to_string) ;
        print_endline (m # to_string) ;
        print_endline (ox # to_string) ;
        print_endline (oz # to_string) ;

        print_endline "\nOk, now let's have some fun:\n" ;

        print_string "water = water -> " ; print_endline (string_of_bool (w # equals w)) ;
        print_string "water = ozone -> " ; print_endline (string_of_bool (w # equals ox))
