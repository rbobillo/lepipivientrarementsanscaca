let () =
    let m = new Alkane.methane in
    let e = new Alkane.ethane in
    let pr = new Alkane.propane in
    let b = new Alkane.butane in
    let pe = new Alkane.pentane in
    let hx = new Alkane.hexane in
    let hp = new Alkane.heptane in
    let o = new Alkane.octane in
        print_endline ( m # to_string ) ;
        print_endline ( e # to_string ) ;
        print_endline ( pr # to_string ) ;
        print_endline ( b # to_string ) ;
        print_endline ( pe # to_string ) ;
        print_endline ( hx # to_string ) ;
        print_endline ( hp # to_string ) ;
        print_endline ( o # to_string ) ;

        print_endline "\nOk, now let's have some fun:\n" ;

        print_string "Methane = Octane  -> " ; print_endline (string_of_bool ( m # equals(o) )) ;
        print_string "Methane = Methane -> " ; print_endline (string_of_bool ( m # equals(m) ))
