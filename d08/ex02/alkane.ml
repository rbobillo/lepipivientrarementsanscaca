let form cn hn = match cn with
    | 1 -> "C" ^ "H" ^ (string_of_int hn)
    | _ -> "C" ^ (string_of_int cn) ^ "H" ^ (string_of_int hn)

class alkane cn =
    object
        val _name     = match cn with
            | 1 -> "methane"
            | 2 -> "ethane"
            | 3 -> "propane"
            | 4 -> "butane"
            | 5 -> "pentane"
            | 6 -> "hexane"
            | 7 -> "heptane"
            | 8 -> "octane"
            | _ -> "unknown"
        val _formula  = form cn (2 * cn + 2)

        method name       = _name

        method formula    = _formula

        method to_string  =
            "Molecule: '" ^ _name ^ "'	-> " ^ _formula

        method equals (comp: alkane) = _formula = comp # formula
    end

class methane = object inherit alkane 1 end
class ethane = object inherit alkane 2 end
class propane = object inherit alkane 3 end
class butane = object inherit alkane 4 end
class pentane = object inherit alkane 5 end
class hexane = object inherit alkane 6 end
class heptane = object inherit alkane 7 end
class octane = object inherit alkane 8 end
