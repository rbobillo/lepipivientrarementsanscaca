let string_of_bool b = match b with
    | true -> "true"
    | _ -> "false"

let () =
    let h = new Atom.h in
    let c = new Atom.c in
    let o = new Atom.o in
    let ar = new Atom.ar in
    let b = new Atom.b in
    let be = new Atom.be in
    let ca = new Atom.ca in
    let he = new Atom.he in
    let k = new Atom.k in
    let n = new Atom.n in
    let ne = new Atom.ne in
        print_endline ( h # to_string ) ;
        print_endline ( c # to_string ) ;
        print_endline ( o # to_string ) ;
        print_endline ( ar # to_string ) ;
        print_endline ( b # to_string ) ;
        print_endline ( be # to_string ) ;
        print_endline ( ca # to_string ) ;
        print_endline ( he # to_string ) ;
        print_endline ( k # to_string ) ;
        print_endline ( n # to_string ) ;
        print_endline ( ne # to_string ) ;

        print_endline "\nOk, now let's have some fun:\n" ;

        print_string "K = Ca  -> " ; print_endline (string_of_bool ( k#equals(ca) )) ;
        print_string "Ca = Ca -> " ; print_endline (string_of_bool ( ca#equals(ca) ))
