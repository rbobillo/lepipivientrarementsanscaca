class virtual atom (n:string) (s:string) (an:int) =
    object ( self )
        val _name: string        = n
        val _symbol: string      = s
        val _atomic_number: int  = an

        method name           = _name
        method symbol         = _symbol
        method atomic_number  = _atomic_number

        method to_string =
            if String.length _name < 6 then
                "Atom of '" ^ _name ^ "'		-> " ^ _symbol ^ ",	#" ^ (string_of_int _atomic_number)
            else
                "Atom of '" ^ _name ^ "'	-> " ^ _symbol ^ ",	#" ^ (string_of_int _atomic_number)

        method equals (comp: atom) =
            _name             = comp # name
            && _symbol        = comp # symbol
            && _atomic_number = comp # atomic_number

    end

class h =
    object
        inherit atom "Hydrogen" "H" 1
    end

class c =
    object
        inherit atom "Carbon" "C" 6
    end

class o =
    object
        inherit atom "Oxygen" "O" 8
    end

class be =
    object
        inherit atom "Beryllium" "Be" 4
    end

class k =
    object
        inherit atom "Potassium" "K" 19
    end

class ca =
    object
        inherit atom "Calcium" "Ca" 20
    end

class b =
    object
        inherit atom "Boron" "B" 5
    end

class n =
    object
        inherit atom "Nitrogen" "N" 7
    end

class he =
    object
        inherit atom "Helium" "He" 2
    end

class ne =
    object
        inherit atom "Neon" "Ne" 10
    end

class ar =
    object
        inherit atom "Argon" "Ar" 18
    end
