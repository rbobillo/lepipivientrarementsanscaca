let get_rand_char () = (String.make 1 (char_of_int (((Random.bits ()) mod 57) + 65)))

class dalek =
    object (self)
        val _name = "Dalek" ^ get_rand_char() ^ get_rand_char() ^ get_rand_char()
        val _hp = 100
        val mutable _shield = true

        method to_string =
            _name ^ " (shield: " ^ (string_of_bool _shield) ^ ", " ^ (string_of_int _hp) ^ "hp left)"

        method talk =
            match ((Random.int 4)) with
            | 0 -> print_endline "Explain! Explain!"
            | 1 -> print_endline "Exterminate! Exterminate!"
            | 2 -> print_endline "I obey!"
            | _ -> print_endline "You are the Doctor! You are the enemy of the Daleks!"

        method exterminate (people : People.people) =
            people # die ;
            (_shield <- (not _shield)) ; ()

        method die =
            match _shield with
            | false -> print_endline "Emergency Temporal Shift!"
            | true -> print_endline "Shield activated: can't be destroyed"
    end
