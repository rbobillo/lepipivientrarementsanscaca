class people name =
    object ( self )

        val _name = name
        val _hp = 100

        initializer print_endline (_name ^ " has come.")

        method to_string =
            name ^ ", people with " ^ (string_of_int _hp) ^ " hp."

        method talk =
            print_endline ("I’m " ^ _name ^ "! Do you know the Doctor?")

        method die =
            print_endline "Aaaarghh!"

    end
