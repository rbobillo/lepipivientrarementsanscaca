let () =
    let sidekick = new People.people "rbobillo" in
    print_endline sidekick # to_string ;
    sidekick # talk ;
    print_endline "" ;
    let doctor = new Doctor.doctor "The Doctor" 2000 sidekick in
        print_endline doctor # to_string ;
        doctor # talk ;
        doctor # travel_in_time 2015 1915 ;
        doctor # use_sonic_screwdriver ;
    sidekick # die ;
