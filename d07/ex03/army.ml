

class ['a, 'b] army (army: 'a) =
object (self)
	val _army = army

	method add (inst: 'b) =
		new army (inst :: _army)
	
	method delete =
	let del l = match l with
	| h :: t -> h#die ; t
	| _ -> []
	in new army (del _army)
	
	method print =
	let rec p l = match l with
	| h :: t -> print_endline h#to_string ; p t
	| [] -> ()
	in p _army
end
