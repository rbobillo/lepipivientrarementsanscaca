class ['a, 'b, 'c] doctor (name:'a) (age:'b) (sidekick:'c) =
    object (self)
        val _name = name
        val _age = age
        val _sidekick = sidekick
        val _hp = 100

        initializer print_endline (_name ^ " has come: " ^ (string_of_int _age) ^ "yo, " ^ (string_of_int _hp) ^ "hp. He's got a cool sidekick (" ^ (_sidekick # to_string) ^ ")")

        method talk =
            print_endline ( "Hi! I’m the Doctor!" )

        method to_string =
            name ^ " is " ^ (string_of_int _age) ^ " with " ^ (string_of_int _hp) ^ "hp left. He's got an awesome sidekick: " ^ _sidekick#to_string

        method travel_in_time start arrival =
            ignore (start - arrival);
            print_endline
"          _
         /-\\
    _____|#|_____
   |_____________|
  |_______________|
|||_POLICE_##_BOX_|||
 | |¯|¯|¯|||¯|¯|¯| |
 | |-|-|-|||-|-|-| |
 | |_|_|_|||_|_|_| |
 | ||~~~| | |¯¯¯|| |
 | ||~~~|!|!| O || |
 | ||~~~| |.|___|| |
 | ||¯¯¯| | |¯¯¯|| |
 | ||   | | |   || |
 | ||___| | |___|| |
 | ||¯¯¯| | |¯¯¯|| |
 | ||   | | |   || |
 | ||___| | |___|| |
|¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯|
 ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯"

        method use_sonic_screwdriver =
            print_endline "Whiiiiwhiiiwhiii Whiiiiwhiiiwhiii Whiiiiwhiiiwhiii"

        method private regenerate =
            new doctor _name _age _sidekick;

        method die = "That sad, " ^ _name ^ "is dead"
    end
