(* COMPILE WITH
    ocamlopt unix.cmxa micronap.ml
*)

let my_sleep () = Unix.sleep 1

let do_sleep n =
    for i=1 to n do
        my_sleep ()
    done

let do_stuff arr =
    try
        do_sleep (int_of_string arr.(1))
    with
    | Invalid_argument err -> ()
    | Failure err -> ()

let () =
    do_stuff Sys.argv
