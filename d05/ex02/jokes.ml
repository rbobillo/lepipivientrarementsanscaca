let jokes = Array.make 6 ""

let init_jokes =
    jokes.(0) <- "Do you know what is not fun at all ?\nA lake." ;
    jokes.(1) <- "What is red, and bad for teeth ?\nA brick." ;
    jokes.(2) <- "A man walks into a bar, his alcoolism problems have torn his family life." ;
    jokes.(3) <- "What is yellow and has big wheels ?\nPee, I lied for the wheels" ;
    jokes.(4) <- "What a hooker and a bicycle have in common ?\nThey both have wheels... Well except for the hooker." ;
    jokes.(5) <- "The bocal is always working hard for us."

let get_joke n =
    Printf.printf "joke #%d: %s\n" n (Array.get jokes n)

let () =
    init_jokes ;
    Random.self_init () ;
    get_joke (Random.int 6)
