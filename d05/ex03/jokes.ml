(* COMPILE WITH
	ocamlopt str.cmxa jokes.ml
*)

let string_of_file f =
    let plen = (String.length (String.concat "" ["#jokefile:";f])) + 1 in
        let ic = open_in f in
            let n = in_channel_length ic in
                let res = String.create n in
                    really_input ic res 0 n;
    close_in ic;
    String.sub res plen ((String.length res) - plen)

let jokes_array str =
    let ls = Str.split (Str.regexp "___\n") str
    in Array.of_list ls

let endof src n =
    String.sub src ((String.length src) - n) n

let is_valid_head f =
    let ic = open_in f in
        try
            input_line ic = String.concat "" ["#jokefile:";f]
        with End_of_file -> close_in ic ; false

let is_valid_file f =
    try
        match endof f.(1) 4 with
        | ".jok" -> is_valid_head f.(1)
        | _ -> false
    with
    | Invalid_argument err -> false
    | Failure err -> false

let get_joke jokes n =
    Printf.printf "joke #%d: %s" n (Array.get jokes n)

let () =
    Random.self_init () ;
    if is_valid_file Sys.argv then
        let f = Sys.argv.(1) in
            let jokes = jokes_array (string_of_file f) in
                get_joke jokes (Random.int (Array.length jokes))
