type 'a ft_ref = { mutable content : 'a }

let return r = { content = r }

let get r = r.content

let set r a = r.content <- a

let bind r f = return (get (f r.content))

let () =
    Printf.printf "Setting a = (ft_ref 7)\n" ;
    let a = (return 7) in
        Printf.printf "a = %d\n" (get a) ;
        Printf.printf "Setting a to 42 -> ( set a 42 )\n" ;
        set a 42 ;
        Printf.printf "a = %d\n" (get a) ;
        Printf.printf "Setting a to 84 -> ( bind a (fun x -> x + x) )\n" ;
        set a (get (bind a (fun x -> return (x + x)))) ;
        Printf.printf "a = %d\n" (get a) ;
