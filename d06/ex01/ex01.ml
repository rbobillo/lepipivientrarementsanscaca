module StringHash =
    struct
        type t = String.t

        let equal i j = (i=j)

        let hash i =
            let rec ft_hash s len res =
                if len >= 0 then ft_hash s (len-1) (res + ((int_of_char (String.get s len)) * (len+1)))
                else res mod 100
            in
            ft_hash i ((String.length i)-1) 0
    end

module StringHashtbl = Hashtbl.Make (StringHash)

let () =
    let ht = StringHashtbl.create 5 in
        let values = ["Hello";"World";"42";"OCaml";"H"] in
            let pairs = List.map (fun s -> (s, StringHash.hash s)) values in
                List.iter (fun (k,v) -> StringHashtbl.add ht k v) pairs;
                StringHashtbl.iter (fun k v -> Printf.printf "k = \"%s\", v = %d\n" k v) ht
