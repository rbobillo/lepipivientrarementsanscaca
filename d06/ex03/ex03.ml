module type FIXED = sig
    type t
    val of_float : float -> t
    val of_int : int -> t
    val to_float : t -> float
    val to_int : t -> int
    val to_string : t -> string
    val zero : t
    val one : t
    val succ : t -> t
    val pred : t -> t
    val min : t -> t -> t
    val max : t -> t -> t
    val gth : t -> t -> bool
    val lth : t -> t -> bool
    val gte : t -> t -> bool
    val lte : t -> t -> bool
    val eqp : t -> t -> bool
    val eqs : t -> t -> bool
    val add : t -> t -> t
    val sub : t -> t -> t
    val mul : t -> t -> t
    val div : t -> t -> t
    val foreach : t -> t -> (t -> unit) -> unit
end

module type FRACTIONAL_BITS = sig
    val bits : int
end


module type MAKE =
    functor (Fractional: FRACTIONAL_BITS) ->
        FIXED

module Make : MAKE =
    functor (Fractional: FRACTIONAL_BITS) ->
        struct
            let bits = Fractional.bits
            type t = int

            let to_float value =
                (float_of_int value) /. (float_of_int (1 lsl bits))

            let to_int value =
                ((value land (lnot (1 lsl 31))) lsr bits) lor (value land (1 lsl 31))

            let of_float fl =
                let roundf f =
                    if ((ceil f) -. f) > (f -. (floor f))
                    then int_of_float f
                    else int_of_float (ceil f)
                in let rec aux f = function
                    | n when n < 1 -> roundf f
                    | n -> aux (f *. 2.0) (n - 1)
                in aux fl bits

            let of_int n =
                (n lsl bits) lor (n land (1 lsl 31))
            let to_string value =
                string_of_float (to_float value)
            let zero = 0
            let one = (of_int 1)
            let succ value = value + 1
            let pred value = value - 1
            let min = min
            let max = max
            let gth = ( > )
            let lth = ( < )
            let gte = ( >= )
            let lte = ( <= )
            let add = ( + )
            let sub = ( - )
            let mul = ( * )
            let div = ( / )
            let eqp = ( == )
            let eqs = ( = )
            let rec foreach x y fn =
                fn x;
                if not (eqs x y) then
                    foreach (if gth x y then pred x else succ x) y fn
        end

module Fixed4 : FIXED = Make (struct let bits = 4 end)
module Fixed8 : FIXED = Make (struct let bits = 8 end)
module Fixed16 : FIXED = Make (struct let bits = 16 end)

let () =
    let string_of_bool x = if x then "true" else "false" in
        let x8 = Fixed8.of_float 21.10 in
            let y8 = Fixed8.of_float 21.32 in
                let r8 = Fixed8.add x8 y8 in
                    print_endline (Fixed8.to_string r8);
                    Fixed4.foreach (Fixed4.zero) (Fixed4.one) (fun f -> print_endline (Fixed4.to_string f));
                    Fixed4.foreach (Fixed4.one) (Fixed4.zero) (fun f -> print_endline (Fixed4.to_string f));
                    print_endline (Fixed4.to_string (Fixed4.add Fixed4.one Fixed4.one));
                    let z8 = Fixed8.of_float 21.32 in
                        print_endline (string_of_bool (Fixed8.eqp y8 z8));
                        print_endline (string_of_bool (Fixed8.eqs y8 z8));
                        print_endline (Fixed16.to_string (Fixed16.of_int 42));
