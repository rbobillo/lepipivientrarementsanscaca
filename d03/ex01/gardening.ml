type 'a tree = Nil | Node of 'a * 'a tree * 'a tree

let draw_square x y size =
    Graphics.moveto (x-(size/2)) (y-(size/2)) ;
    Graphics.lineto (x-(size/2)) (y+(size/2)) ;
    Graphics.moveto (x-(size/2)) (y+(size/2)) ;
    Graphics.lineto (x+(size/2)) (y+(size/2)) ;
    Graphics.moveto (x+(size/2)) (y+(size/2)) ;
    Graphics.lineto (x+(size/2)) (y-(size/2)) ;
    Graphics.moveto (x+(size/2)) (y-(size/2)) ;
    Graphics.lineto (x-(size/2)) (y-(size/2))

let draw_tree_node n =
    if n = Nil
    then (draw_square 480 300 50 ; Graphics.moveto 472 290 ; Graphics.draw_string "Nil")
    else
    (
        draw_square 480 300 50 ;
        Graphics.moveto 466 295 ;
        Graphics.draw_string "value" ;
        Graphics.moveto 505 300 ;
        Graphics.lineto 600 375 ;
        Graphics.moveto 505 300 ;
        Graphics.lineto 600 225 ;
        draw_square 625 375 50 ;
        Graphics.moveto 617 370 ;
        Graphics.draw_string "Nil" ;
        draw_square 625 225 50 ;
        Graphics.moveto 617 220 ;
        Graphics.draw_string "Nil"
    )

let rec count t c = match t with
    | Node (_ , l, r) -> (count l (count r (c+1)))
    | Nil -> c

let size t =
    count t 0

let get_greater cl cr =
    if cr > cl
    then cr
    else
    cl

let rec count_height t c = match t with
    | Node (_ , l, r) -> get_greater (count l (c+1)) (count r (c+1))
    | Nil -> c

let height t =
    count_height t 0

let get_type n = match n with
    | Node (v, _, _) -> v
    | Nil -> "Nil"


let rec draw t x y m =
    draw_square x y 30 ;
    Graphics.moveto (x - 13) y ;
    Graphics.draw_string (get_type t) ;
    if (get_type t) <> "Nil" then (
        Graphics.moveto (x + 15) y ;
        Graphics.lineto (x + 60) (y + m) ;
        Graphics.moveto (x + 15) y ;
        Graphics.lineto (x + 60) (y - m) ;
        let rec loop t = match t with
            | Node (_, n, n2) -> draw n (x + 75) (y + m) (m - 20) ; draw n2 (x + 75) (y - m) (m - 10)
            | Nil -> ()
        in loop t
    ) else ()

let draw_tree t =
    draw t 250 300 75

(* **** *)

let rec loop () =
    if (int_of_char (Graphics.read_key ())) = 27
    then ()
    else
    (loop ())

let () =
    Graphics.open_graph " 960x540" ;
    draw_tree (Node ("42", Node ("41", Nil, Nil), Node ("43", Nil, Nil))) ;
    loop () ;
    Graphics.clear_graph () ;
    draw_tree (Node ("42", Node ("41", Node ("41", Nil, Nil), Nil), Node ("43", Nil, Node ("41", Nil, Nil)))) ;
    loop () ;
    Graphics.clear_graph () ;
    draw_tree (Node ("42", Node ("41", Node ("41", Nil, Nil), Nil), Node ("43", Nil, Node ("41", Node ("41", Node ("41", Nil, Nil), Nil), Nil)))) ;
    loop () ;
    Graphics.close_graph () ;
    print_string "test with a tree of size 3 : " ;
    print_int (size (Node ("42", Node ("41", Nil, Nil), Node ("43", Nil, Nil)))) ;
    print_char '\n';
    print_string "test with a tree of size 5 : " ;
    print_int (size (Node ("42", Node ("41", Node ("41", Nil, Nil), Nil), Node ("43", Nil, Node ("41", Nil, Nil))))) ;
    print_char '\n';
    print_string "test with a tree of height 2 : " ;
    print_int (height (Node ("42", Node ("41", Nil, Nil), Node ("43", Nil, Nil)))) ;
    print_char '\n';
    print_string "test with a tree of height 3 : " ;
    print_int (height (Node ("42", Node ("41", Node ("41", Nil, Nil), Nil), Node ("43", Nil, Node ("41", Nil, Nil))))) ;
    print_char '\n';
    print_string "test with a tree of height 6 : " ;
    print_int (height (Node ("42", Node ("41", Node ("41", Nil, Nil), Nil), Node ("43", Nil, Node ("41", Node ("41", Node ("41", Nil, Node ("41", Nil, Nil)), Nil), Nil))))) ;
    print_char '\n';
