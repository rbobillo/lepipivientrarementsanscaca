type 'a tree = Nil | Node of 'a * 'a tree * 'a tree

let draw_square x y size =
    Graphics.moveto (x-(size/2)) (y-(size/2)) ;
    Graphics.lineto (x-(size/2)) (y+(size/2)) ;
    Graphics.moveto (x-(size/2)) (y+(size/2)) ;
    Graphics.lineto (x+(size/2)) (y+(size/2)) ;
    Graphics.moveto (x+(size/2)) (y+(size/2)) ;
    Graphics.lineto (x+(size/2)) (y-(size/2)) ;
    Graphics.moveto (x+(size/2)) (y-(size/2)) ;
    Graphics.lineto (x-(size/2)) (y-(size/2))

let draw_tree_node n =
    if n = Nil
    then (draw_square 480 300 50 ; Graphics.moveto 472 290 ; Graphics.draw_string "Nil")
    else
    (
        draw_square 480 300 50 ;
        Graphics.moveto 466 295 ;
        Graphics.draw_string "value" ;
        Graphics.moveto 505 300 ;
        Graphics.lineto 600 375 ;
        Graphics.moveto 505 300 ;
        Graphics.lineto 600 225 ;
        draw_square 625 375 50 ;
        Graphics.moveto 617 370 ;
        Graphics.draw_string "Nil" ;
        draw_square 625 225 50 ;
        Graphics.moveto 617 220 ;
        Graphics.draw_string "Nil"
    )

let rec count t c = match t with
    | Node (_ , l, r) -> (count l (count r (c+1)))
    | Nil -> c

let size t =
    count t 0

let get_greater cl cr =
    if cr > cl
    then cr
    else
    cl

let rec count_height t c = match t with
    | Node (_ , l, r) -> get_greater (count l (c+1)) (count r (c+1))
    | Nil -> c

let height t =
    count_height t 0

(******************************************************************************)

let rec loop () =
    if (int_of_char (Graphics.read_key ())) = 27
    then ()
    else
    (loop ())

let () =
    let h = 800 in let w = 600 in
    Graphics.open_graph (" " ^ (string_of_int h) ^ "x" ^ (string_of_int w)) ;
    draw_square ((h/2) + (h mod 2)) ((w/2) + (w mod 2)) 100 ;
    loop () ;
    Graphics.clear_graph () ;
    draw_tree_node (Node ("42", Node ("41", Node ("41", Nil, Nil), Nil), Node ("43", Nil, Node ("41", Nil, Nil)))) ;
    loop()
