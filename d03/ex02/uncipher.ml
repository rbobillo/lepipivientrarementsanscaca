let rec uncaesar s n =
    let unrot c =
        if c = 'a' then
            'z'
        else if c = 'A' then
            'Z'
        else if (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') then
            ( char_of_int (int_of_char c - 1) )
        else
            c
    in
        if n > 0 then
            ( uncaesar (String.map unrot s) (n - 1) )
        else
            s

let unrot42 s =
    uncaesar s 42

let rec x_string s k =
    let rot c = char_of_int ( (int_of_char c) lxor k)
    in ( String.map rot s )

let xor s key = match key with
    | 0 -> s
    | _ -> x_string s key

let unxor s key = xor s key

let ft_uncrypt s f = match s with
    | "" -> ""
    | _ -> f s

(******************************************************************************)

let () =
    Printf.printf "%s\n" ( ft_uncrypt "zu ikyi kdu fehju" ( fun s -> unrot42 s ) )
