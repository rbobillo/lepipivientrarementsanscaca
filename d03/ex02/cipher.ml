let rec caesar s n = (* IL FAUT AUSSI GERER LE CAS OU N EST NEGATIF *)
    let rot c =
        if c = 'z' then
            'a'
        else if c = 'Z' then
            'A'
        else if (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') then
            ( char_of_int (int_of_char c + 1) )
        else
            c
    in
        if n > 0 then
            ( caesar (String.map rot s) (n - 1) )
        else
            s

let rot42 s =
    caesar s 42

let x_string s k =
    let rot c = char_of_int ( (int_of_char c) lxor k)
    in ( String.map rot s )

let xor s key = match key with
    | 0 -> s
    | _ -> x_string s key

let ft_crypt s f = match s with
    | "" -> ""
    | _ -> f s

(******************************************************************************)

let () =
    Printf.printf "%s\n" ( ft_crypt "je suis une porte" ( fun s -> rot42 s ) )
