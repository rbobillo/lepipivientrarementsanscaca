module Watchtower =
    struct
        type hour = int

        let zero = 0

        let add (h1:hour) (h2:hour) =
            (h1 + h2) mod 12

        let sub (h1:hour) (h2:hour) =
            let res = (h1 - h2) mod 12 in
                if res < 0 then (12 + res) mod 12
                else res
    end
