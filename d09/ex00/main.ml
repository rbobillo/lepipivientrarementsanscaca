let () =
    let ( ++ ) = Watchtower.Watchtower.add in
    let ( -- ) = Watchtower.Watchtower.sub in
        let start = (Watchtower.Watchtower.zero mod 12) in
        let sp5 = (start ++ 5) in
        let sp12 = (start ++ 12) in
        let sp20 = (start ++ 20) in
        let sm5 = (start -- 5) in
        let sm12 = (start -- 12) in
        let sm20 = (start -- 20) in
            Printf.printf "start:        %d\n\n" start ;
            Printf.printf "start + 5:    %d\n" sp5 ;
            Printf.printf "start + 12:   %d\n" sp12 ;
            Printf.printf "start + 20:   %d\n\n" sp20 ;
            Printf.printf "start - 5:    %d\n" sm5 ;
            Printf.printf "start - 12:   %d\n" sm12 ;
            Printf.printf "start - 20:   %d\n" sm20
