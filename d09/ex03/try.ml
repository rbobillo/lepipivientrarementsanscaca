module Try =
    struct
        type 'a t = Success of 'a
                  | Failure of exn

        let return thingie = Success thingie

        let bind e fn =
            match e with
            | Success value -> (try fn value with exc -> Failure exc)
            | Failure exc -> Failure exc

        let recover e fn =
            match e with
            | Failure exc -> fn exc
            | success -> success

        exception Filtered of string

        let filter e predicate =
            match e with
            | Success value as success ->
                    if predicate value then success
                    else Failure (Filtered "Value did not satisfy the given predicate")
            | fail -> fail

        let flatten = function
            | Success (Success value) -> Success value
            | Success (Failure exc  ) -> Failure exc
            | Failure exc -> Failure exc
    end
