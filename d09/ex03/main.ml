let () =
    let print_try t =
        match t with
        | Try.Try.Success v -> print_int v ; print_newline ()
        | Try.Try.Failure v -> print_endline @@ Printexc.to_string v in
    let tryInt = Try.Try.return 42 in
    let byTwo = Try.Try.bind tryInt (fun x -> Try.Try.return @@ x / 2) in
    let byZero = Try.Try.bind byTwo (fun x -> Try.Try.return @@ x / 0) in
    let recoveredByZero = Try.Try.recover byZero (fun exc -> Try.Try.return 0) in
    let lowerThan5 = Try.Try.filter recoveredByZero (fun x -> x < 5 ) in
    let greaterThan10 = Try.Try.filter lowerThan5 (fun x -> x > 10 ) in
    print_endline "lifted 42 in the monad: ";
    print_try tryInt;
    print_endline "divided by 2: ";
    print_try byTwo;
    print_endline "divided by 0: ";
    print_try byZero;
    print_endline "recovered failure, set to 0: ";
    print_try recoveredByZero;
    print_endline "is lower than 5: ";
    print_try lowerThan5;
    print_endline "is greater than 10: ";
    print_try greaterThan10;
    print_endline "flattened success(exn): ";
    print_try @@ Try.Try.flatten @@ Try.Try.return byZero;
    print_endline "flattened success(success): ";
    print_try @@ Try.Try.flatten @@ Try.Try.return tryInt;
