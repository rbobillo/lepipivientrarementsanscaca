module type MONOID =
    sig
        type element
        val zero1 : element
        val zero2 : element
        val mul : element -> element -> element
        val add : element -> element -> element
        val div : element -> element -> element
        val sub : element -> element -> element
    end

module INT =
    struct
        type element = int
        let zero1 : element = 0
        let zero2 : element = 1
        let add : element -> element -> element = ( + )
        let sub : element -> element -> element = ( - )
        let mul : element -> element -> element = ( * )
        let div : element -> element -> element = ( / )
    end

module FLOAT =
    struct
        type element = float
        let zero1 : element = 0.0
        let zero2 : element = 1.0
        let add : element -> element -> element = ( +. )
        let sub : element -> element -> element = ( -. )
        let mul : element -> element -> element = ( *. )
        let div : element -> element -> element = ( /. )
    end


module Calc =
    functor (M : MONOID) ->
        struct
            let add = M.add
            let sub = M.sub
            let mul = M.mul
            let div = M.div
            let rec power : M.element -> int -> M.element =
                fun n -> function
                    | l when l < 1 -> M.zero2
                    | g -> mul n @@ power n @@ g - 1
            let rec fact : M.element -> M.element =
                function
                    | n when n <= M.zero2 -> M.zero2
                    | n -> mul n @@ fact @@ sub n M.zero2
        end


module Calc_int = Calc(INT)
module Calc_float = Calc(FLOAT)
