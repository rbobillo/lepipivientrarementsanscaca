module App =
    struct
        type project = (string * string * int)

        let zero : project = ("", "", 0)

        let combine : project -> project -> project =
            fun (lname, _, lgrade) (rname, _, rgrade) ->
                let avgGrade = (lgrade + rgrade) / 2 in
                let status = if avgGrade > 80 then "succeed" else "failed" in
                (lname ^ rname, status, avgGrade)

        let fail : project -> project =
            fun (name, _, _) ->
                (name, "failed", 0)

        let success : project -> project =
            fun (name, _, _) ->
                (name, "succeed", 80)

        let print_proj : project -> unit =
            fun (name, status, grade) ->
                print_string ("Project (" ^ name ^ ", " ^ status ^ ", " ^ (string_of_int grade) ^ ")")
    end
