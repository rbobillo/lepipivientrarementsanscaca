let _length   = 1400
let _height   = 900
let _lcenter  = ((_length/2) + (_length mod 2))
let _hcenter  = ((_height/2) + (_height mod 2))

let ( ~~ ) x   = _length * x / 100 (* position relative sur l'axe des abscisses *)
let ( ~~~ ) x  = _height * x / 100 (* position relative sur l'axe des ordonnees *)

(******************************************************************************)

let draw_rectangle x y len h =
    Graphics.moveto (x-(len/2)) (y-(h/2)) ;
    Graphics.lineto (x-(len/2)) (y+(h/2)) ;
    Graphics.moveto (x-(len/2)) (y+(h/2)) ;
    Graphics.lineto (x+(len/2)) (y+(h/2)) ;
    Graphics.moveto (x+(len/2)) (y+(h/2)) ;
    Graphics.lineto (x+(len/2)) (y-(h/2)) ;
    Graphics.moveto (x+(len/2)) (y-(h/2)) ;
    Graphics.lineto (x-(len/2)) (y-(h/2))

let fill_rectangle x y len h percent =
    let plen = (len * percent / 100) in
    for n=0 to plen do
        Graphics.moveto (x-(len/2)+n) (y-(h/2)) ;
        Graphics.lineto (x-(len/2)+n) (y+(h/2)) ;
    done

let put_text x y str =
    Graphics.set_line_width (4);
    Graphics.moveto x y ;
    Graphics.draw_string str

let refresh state =
    Graphics.clear_graph () ;

    put_text (~~19) (~~~90) "HEALTH"       ; draw_rectangle (~~20) (~~~85) 200 50
                                           ; fill_rectangle (~~20) (~~~85) 200 50 state#health ;

    put_text (~~39) (~~~90) "ENERGY"       ; draw_rectangle (~~40) (~~~85) 200 50
                                           ; fill_rectangle (~~40) (~~~85) 200 50 state#energy ;

    put_text (~~58) (~~~90) " HYGIENE"     ; draw_rectangle (~~60) (~~~85) 200 50
                                           ; fill_rectangle (~~60) (~~~85) 200 50 state#hygiene ;

    put_text (~~78) (~~~90) " HAPPINESS"   ; draw_rectangle (~~80) (~~~85) 200 50
                                           ; fill_rectangle (~~80) (~~~85) 200 50 state#happiness ;

    put_text (~~19) (~~~14) " EAT"         ; draw_rectangle (~~20) (~~~15) 150 100;
    put_text (~~38) (~~~14) " THUNDER"     ; draw_rectangle (~~40) (~~~15) 150 100;
    put_text (~~59) (~~~14) "BATH"         ; draw_rectangle (~~60) (~~~15) 150 100;
    put_text (~~79) (~~~14) " KILL"        ; draw_rectangle (~~80) (~~~15) 150 100

(******************************************************************************)

let wait milli =
  let sec = milli /. 1000. in
  let tm1 = Unix.gettimeofday () in
    let rec loop () =
        if ((Unix.gettimeofday ()) -. tm1) < sec then loop ()
        else ()
    in loop ()

let rec death_screen () =
    Graphics.set_line_width (100);
    Graphics.set_text_size (200) ;
    Graphics.moveto 700 500 ;
    Graphics.draw_string "GAME OVER" ;
    if (Graphics.key_pressed ()) then
        begin
            if (int_of_char (Graphics.read_key ())) = 27 then ()
        end
    else death_screen ()

let draw_tard turn =
    draw_rectangle (~~50) (~~33) 250 250 ;
    Graphics.draw_circle (~~50) (~~46) 50 ;
    if (turn < 10) then (
        Graphics.draw_circle (~~49) (~~48) 12 ;
        Graphics.draw_circle (~~51) (~~48) 8 ;
        Graphics.moveto 575 550 ;
        Graphics.lineto 500 600 ;
        Graphics.moveto 825 550 ;
        Graphics.lineto 895 600
    ) else (
        Graphics.draw_circle (~~49) (~~48) 8 ;
        Graphics.draw_circle (~~51) (~~48) 12 ;
        Graphics.moveto 575 550 ;
        Graphics.lineto 500 500 ;
        Graphics.moveto 825 550 ;
        Graphics.lineto 895 500
    ) ;
    Graphics.draw_circle (~~50) (~~45) 20 ;
    Graphics.moveto 630 335 ;
    Graphics.lineto 600 250 ;
    Graphics.moveto 770 335 ;
    Graphics.lineto 800 250

let rec loop time state turn =
    if state#health <= 0 then
    begin
        Graphics.clear_graph () ; death_screen () ;
    end
    else (
    refresh state ;
    draw_tard turn ;
    wait 20.0 ;
    let t = time in
    if (Unix.gettimeofday () -. !t) >= 1.0 then
        begin
            state#change_health (-1) ;
            t := (Unix.gettimeofday ()) ;
        end ;
    if (Graphics.key_pressed ()) then
        begin
            if (int_of_char (Graphics.read_key ())) = 27 then ()
        end ;
    if (Graphics.button_down ()) = true
        then (
            ignore (Graphics.wait_next_event [Graphics.Button_up]) ;
            let read_in (x, y) =
                if x > 200 && x < 350 && y < 180 && y > 85 then
                    (state#eat; Graphics.sound 440 1)
                else if x > 450 && x < 630 && y < 180 && y > 85 then
                    (state#thunder; Graphics.sound 415 1)
                else if x > 765 && x < 915 && y < 180 && y > 85 then
                    (state#bath; Graphics.sound 554 1)
                else if x > 1050 && x < 1200 && y < 180 && y > 85 then
                    (state#kill; Graphics.sound 200 1)
            in read_in (Graphics.mouse_pos ()) ;
            loop t state ((turn + 1) mod 20)
    )
    else (loop t state ((turn + 1) mod 20)))

let show_state health energy hygiene happiness =
    Graphics.open_graph (" " ^ (string_of_int _length) ^ "x" ^ (string_of_int _height)) ;
    Graphics.set_window_title "TamagoNiktarass" ;

    let state = new State.state health energy hygiene happiness in
    refresh state ;

    loop (ref (Unix.gettimeofday ())) state 0;
