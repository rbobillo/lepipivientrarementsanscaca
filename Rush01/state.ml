let ( ++ ) x y =
    let res = x + y in
        if res < 1 then 0
        else if res > 100 then 100
        else res

class state (he:int) (en:int) (hy:int) (ha:int) =
    object ( self )

        val mutable _health: int     = he
        val mutable _energy: int     = en
        val mutable _hygiene: int    = hy
        val mutable _happiness: int  = ha

(********************************** GETTERS ***********************************)

        method health     = _health
        method energy     = _energy
        method hygiene    = _hygiene
        method happiness  = _happiness

(********************************** SETTERS ***********************************)

        method change_health x     = _health <- (_health ++ x)
        method change_energy x     = _energy <- (_energy ++ x)
        method change_hygiene x    = _hygiene <- (_hygiene ++ x)
        method change_happiness x  = _happiness <- (_happiness ++ x)

(*************************** METHODES OBLIGATOIRES ****************************)

        method eat      = (
            _health <- (_health ++ 25);
            _energy <- (_energy ++ (-10));
            _hygiene <- (_hygiene ++ (-20));
            _happiness <- (_happiness ++ 5)
        )

        method thunder  = (
            _health <- (_health ++ (-20));
            _energy <- (_energy ++ 25);
            _hygiene <- (_hygiene ++ 0);
            _happiness <- (_happiness ++ (-20))
        )

        method bath    = (
            _health <- (_health ++ (-20));
            _energy <- (_energy ++ (-10));
            _hygiene <- (_hygiene ++ 25);
            _happiness <- (_happiness ++ 5)
        )

        method kill    = (
            _health <- (_health ++ (-20));
            _energy <- (_energy ++ (-10));
            _hygiene <- (_hygiene ++ 0);
            _happiness <- (_happiness ++ 20)
        )

    end
